package com.telerikacademy.team10.safety_car.services;


import com.telerikacademy.team10.safety_car.controllers.rest.FileController;
import com.telerikacademy.team10.safety_car.exceptions.FileNotFoundException;
import com.telerikacademy.team10.safety_car.exceptions.FileStorageException;
import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.repositories.contracts.FileRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class FileServiceImplTest {

    private static final String SCHEME = "http";
    private static final String HOST = "localhost";
    private static final int PORT = 8001;

    private final FileRepository fileRepository = mock(FileRepository.class);

    private final FileServiceImpl sut = new FileServiceImpl(fileRepository);

    @BeforeAll
    public static void setup() {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        when(mockRequest.getScheme()).thenReturn(SCHEME);
        when(mockRequest.getServerName()).thenReturn(HOST);
        when(mockRequest.getServerPort()).thenReturn(PORT);

        RequestAttributes attributes = new ServletRequestAttributes(mockRequest);
        RequestContextHolder.setRequestAttributes(attributes);
    }

    @Test
    void loadFileAsResource_Should_thrownNotFoundException_When_repositoryReturnEmptyResourceWithGivenName() {
        final String fileName = "someFileName";
        when(fileRepository.getAsResource(fileName)).thenReturn(Optional.empty());

        Assertions.assertThrows(
                FileNotFoundException.class,
                () -> sut.loadFileAsResource(fileName)
        );
    }

    @Test
    void loadFileAsResource_Should_returnResourceFromRepository_When_repositoryReturnResourceWithGivenName() {
        final String fileName = "someFileName";
        final Resource expectedResource = mock(Resource.class);
        when(fileRepository.getAsResource(fileName)).thenReturn(Optional.of(expectedResource));

        final Resource result = sut.loadFileAsResource(fileName);

        Assertions.assertEquals(expectedResource, result);
    }


    @Test
    void storeImage_Should_throwInvalidArgumentException_When_givenFileDoesntHaveContentType() {
        final MultipartFile input = mock(MultipartFile.class);
        when(input.getContentType()).thenReturn(null);

        Assertions.assertThrows(
                InvalidArgumentException.class,
                () -> sut.storeImage(input)
        );
    }

    @Test
    void storeImage_Should_throwInvalidArgumentException_When_givenFileHasMusicContentType() {
        final MultipartFile input = mock(MultipartFile.class);
        when(input.getContentType()).thenReturn("audio/mpeg");

        Assertions.assertThrows(
                InvalidArgumentException.class,
                () -> sut.storeImage(input)
        );
    }

    @Test
    void storeImage_Should_throwInvalidArgumentException_When_givenFileHasNoOriginalFilename() {
        final MultipartFile input = mock(MultipartFile.class);
        when(input.getContentType()).thenReturn("image/png");
        when(input.getOriginalFilename()).thenReturn(null);

        Assertions.assertThrows(
                InvalidArgumentException.class,
                () -> sut.storeImage(input)
        );
    }

    @Test
    void storeImage_Should_throwFileStorageException_When_repositoryReturnEmptyUploadedFileNameWithGivenFile() {
        final MultipartFile input = mock(MultipartFile.class);
        when(input.getContentType()).thenReturn("image/png");
        when(input.getOriginalFilename()).thenReturn(anyString());
        when(fileRepository.saveFile(input)).thenReturn(Optional.empty());

        Assertions.assertThrows(
                FileStorageException.class,
                () -> sut.storeImage(input)
        );
    }

    @Test
    void storeImage_Should_returnFileUrlAsString_When_repositoryReturnUploadedFileNameWithGivenFile() {
        final MultipartFile input = mock(MultipartFile.class);
        final String fileName = "someFileName";
        when(input.getOriginalFilename()).thenReturn(fileName);
        when(input.getContentType()).thenReturn("image/jpeg");
        when(fileRepository.saveFile(input)).thenReturn(Optional.of(fileName));
        final String expectedFileUrlAsString = String.format("%s%s", constructFileDistributionPath(), fileName);

        final String result = sut.storeImage(input);

        Assertions.assertEquals(expectedFileUrlAsString, result);
    }

    private static String constructFileDistributionPath() {
        return SCHEME + "://" + HOST + ":" + PORT + FileController.ENDPOINT_FILE_DISTRIBUTION;
    }
}