package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.safety_car.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.safety_car.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.repositories.contracts.AuthenticationRepository;
import com.telerikacademy.team10.safety_car.repositories.contracts.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {

    @InjectMocks
    CustomerServiceImpl customerService;
    @Mock
    CustomerRepository mockCustomerRepository;
    @Mock
    AuthenticationRepository mockAuthenticationRepository;

    @Test
    void getByEmail_should_throwException_when_findByIdIsNotValid() {
        //Arrange
        Customer testCustomer = new Customer();
        when(mockCustomerRepository.findById("email"))
                .thenReturn(Optional.empty());

        //Avt, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> customerService.getByEmail("email"));
    }

    @Test
    void getByEmail_should_returnCustomer_when_findByIdIsValid() {
        //Arrange
        Customer testCustomer = new Customer();
        when(mockCustomerRepository.findById("email"))
                .thenReturn(Optional.of(testCustomer));

        //Act
        Customer result = customerService.getByEmail("email");

        //Assert
        Assertions.assertEquals(testCustomer, result);
    }

    @Test
    void create_should_throwDuplicateEntityException_when_customerExists() {
        //Arrange
        Customer testCustomer = new Customer();
        when(mockAuthenticationRepository.exists(testCustomer.getEmail()))
                .thenReturn(true);

        //Avt, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> customerService.create(testCustomer));
    }

    @Test
    void create_should_createCustomer_when_customerDoesNotExists() {
        //Arrange
        Customer testCustomer = new Customer();

        //Act
        customerService.create(testCustomer);

        //Assert
        Mockito.verify(mockCustomerRepository,
                Mockito.times(1)).save(testCustomer);
    }

    @Test
    void update_should_throwException_when_isAuthenticatedIsFalse() {
        //Arrange
        Customer testCustomer = new Customer();
        when(mockAuthenticationRepository.isAuthenticated("email"))
                .thenReturn(false);

        //Avt, Assert
        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> customerService.update("email", testCustomer));
    }

    @Test
    void update_should_updateCustomer_when_isAuthenticatedIsTrue() {
        //Arrange
        Customer testCustomer = new Customer();
        when(mockAuthenticationRepository.isAuthenticated("email"))
                .thenReturn(true);
        when(mockCustomerRepository.findById("email"))
                .thenReturn(Optional.of(testCustomer));

        //Act
        customerService.update("email", testCustomer);

        //Assert
        Mockito.verify(mockCustomerRepository,
                Mockito.times(1)).save(testCustomer);
    }

    @Test
    void getPolicies_should_throwException_when_customerNotFound() {
        //Arrange
        when(mockCustomerRepository.findById("email"))
                .thenReturn(Optional.empty());

        //Avt, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> customerService.getPolicies("email"));
    }
}