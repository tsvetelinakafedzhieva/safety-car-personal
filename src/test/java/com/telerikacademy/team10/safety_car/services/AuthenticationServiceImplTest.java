package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.safety_car.models.enums.RoleType;
import com.telerikacademy.team10.safety_car.repositories.contracts.AuthenticationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.*;

class AuthenticationServiceImplTest {

    private final AuthenticationRepository authenticationRepository = mock(AuthenticationRepository.class);

    private final AuthenticationServiceImpl sut = new AuthenticationServiceImpl(authenticationRepository);

    @Test
    void register_Should_throwDuplicateEntityException_When_usernameAlreadyExists() {
        String username = "username@abv.bg";
        String password = "pass1";
        when(authenticationRepository.exists(username)).thenReturn(true);

        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> sut.register(username, password)
        );
    }

    @Test
    void register_Should_createUser_When_usernameDoesNotExist() {
        String username = "username@abv.bg";
        String password = "pass1";
        RoleType role = RoleType.ROLE_CUSTOMER;
        when(authenticationRepository.exists(username)).thenReturn(false);

        sut.register(username, password);

        verify(authenticationRepository).create(username, password, role);
    }

    @Test
    void isAuthenticated_Should_returnTrue_When_userIsAuthenticated() {
        String username = "username@abv.bg";
        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);

        Assertions.assertTrue(sut.isAuthenticated(username));
    }

    @Test
    void isAuthenticated_Should_returnFalse_When_userIsNotAuthenticated() {
        String username = "username@abv.bg";
        when(authenticationRepository.isAuthenticated(username)).thenReturn(false);

        Assertions.assertFalse(sut.isAuthenticated(username));
    }

    @Test
    void isAgent_Should_returnTrue_When_userIsAgent() {
        String username = "username@abv.bg";
        Set<RoleType> types = new HashSet<>();
        types.add(RoleType.ROLE_AGENT);
        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);
        when(authenticationRepository.getRoles(username)).thenReturn(types);

        Assertions.assertTrue(sut.isAgent(username));
    }

    @Test
    void isAgent_Should_returnFalse_When_userIsCustomer() {
        String username = "username@abv.bg";
        Set<RoleType> types = new HashSet<>();
        types.add(RoleType.ROLE_CUSTOMER);
        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);
        when(authenticationRepository.getRoles(username)).thenReturn(types);

        Assertions.assertFalse(sut.isAgent(username));
    }

    @Test
    void isAgent_Should_returnFalse_When_userIsNotAuthenticated() {
        String username = "username@abv.bg";
        when(authenticationRepository.isAuthenticated(username)).thenReturn(false);

        Assertions.assertFalse(sut.isAgent(username));
    }

    @Test
    void isCustomer_Should_returnTrue_When_userIsCustomer() {
        String username = "username@abv.bg";
        Set<RoleType> types = new HashSet<>();
        types.add(RoleType.ROLE_CUSTOMER);
        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);
        when(authenticationRepository.getRoles(username)).thenReturn(types);

        Assertions.assertTrue(sut.isCustomer(username));

    }

    @Test
    void isCustomer_Should_returnFalse_When_userIsAgent() {
        String username = "username@abv.bg";
        Set<RoleType> types = new HashSet<>();
        types.add(RoleType.ROLE_AGENT);
        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);
        when(authenticationRepository.getRoles(username)).thenReturn(types);

        Assertions.assertFalse(sut.isCustomer(username));
    }

    @Test
    void isCustomer_Should_returnFalse_When_userIsNotAuthenticated() {
        String username = "username@abv.bg";
        when(authenticationRepository.isAuthenticated(username)).thenReturn(false);

        Assertions.assertFalse(sut.isCustomer(username));
    }

    @Test
    void changePassword_Should_throwUnauthorizedAccessException_When_userIsNotAuthenticated() {
        String username = "username@abv.bg";
        String newPassword = "pass1";
        String oldPassword = "pass2";
        String passwordConfirm = "pass2";

        when(authenticationRepository.isAuthenticated(username)).thenReturn(false);

        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> sut.changePassword(username, oldPassword, newPassword, passwordConfirm));
    }

    @Test
    void changePassword_Should_throwUnauthorizedAccessException_When_userPasswordIsEmpty() {
        String username = "username@abv.bg";
        String newPassword = "pass1";
        String oldPassword = "pass2";
        String passwordConfirm = "pass2";

        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);
        when(authenticationRepository.getPasswordFor(username)).thenReturn(Optional.empty());

        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> sut.changePassword(username, oldPassword, newPassword, passwordConfirm));

    }

    @Test
    void changePassword_Should_throwUnauthorizedAccessException_When_userPasswordDoesNotMatchWithTheGivenOldPassword() {
        String username = "username@abv.bg";
        String originalPassword = "not the same";
        String newPassword = "pass1";
        String oldPassword = "pass2";
        String passwordConfirm = "pass2";

        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);
        when(authenticationRepository.getPasswordFor(username)).thenReturn(Optional.of(originalPassword));

        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> sut.changePassword(username, oldPassword, newPassword, passwordConfirm));

    }

    @Test
    void changePassword_Should_throwInvalidArgumentException_When_userNewPasswordIsNotEqualToPasswordConfirmation() {
        String username = "username@abv.bg";
        String newPassword = "pass1";
        String oldPassword = "pass2";
        String passwordConfirm = "not the same";

        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);
        when(authenticationRepository.getPasswordFor(username)).thenReturn(Optional.of(oldPassword));

        Assertions.assertThrows(InvalidArgumentException.class,
                () -> sut.changePassword(username, oldPassword, newPassword, passwordConfirm));
    }

    @Test
    void changePassword_Should_updatePassword_When_userInputForChangingPasswordIsValid() {
        String username = "username@abv.bg";
        String newPassword = "pass1";
        String passwordConfirm = "pass1";
        String oldPassword = "pass2";
        when(authenticationRepository.isAuthenticated(username)).thenReturn(true);
        when(authenticationRepository.getPasswordFor(username)).thenReturn(Optional.of(oldPassword));

        sut.changePassword(username, oldPassword, newPassword, passwordConfirm);

        verify(authenticationRepository).updatePassword(oldPassword, newPassword);
    }


}