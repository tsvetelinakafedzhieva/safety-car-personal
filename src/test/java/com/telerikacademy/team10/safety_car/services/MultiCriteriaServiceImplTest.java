package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.models.MultiCriteria;
import com.telerikacademy.team10.safety_car.models.OfferRequest;
import com.telerikacademy.team10.safety_car.models.Premium;
import com.telerikacademy.team10.safety_car.repositories.contracts.MultiCriteriaRepository;
import com.telerikacademy.team10.safety_car.utility.DateUtility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.*;

import static org.mockito.Mockito.*;

class MultiCriteriaServiceImplTest {

    private static final double RESULT_COMPARISON_DELTA = 0.0001;

    private static final double TAX_COEFFICIENT = 1.1;
    private static final double AGE_COEFFICIENT = 1.05;
    private static final double ACCIDENT_COEFFICIENT = 1.2;

    private final MultiCriteriaRepository multiCriteriaRepository = mock(MultiCriteriaRepository.class);

    private final MultiCriteriaServiceImpl sut = new MultiCriteriaServiceImpl(multiCriteriaRepository);

    @Test
    void calculateTotalPremium_Should_throwInvalidArgumentException_When_repositoryReturnEmpty() {
        final int carAge = 12;
        final int carCc = 1500;
        final int driverAge = 24;
        final boolean hasAccidents = false;
        final Date firstRegistrationDate = DateUtility.convertToDate(LocalDate.now().minusYears(carAge));
        final OfferRequest input = new OfferRequest(firstRegistrationDate, carCc, driverAge, hasAccidents);
        when(multiCriteriaRepository.getMultiCriteria(input.getCubicCapacity(), carAge)).thenReturn(Optional.empty());

        Assertions.assertThrows(
                InvalidArgumentException.class,
                () -> sut.calculateTotalPremium(input)
        );
    }

    @Test
    void calculateTotalPremium_Should_returnPremiumWithValue_When_repositoryReturnMultiCriteria_And_driverAgeIs_24() {
        final int carAge = 12;
        final int carCc = 1500;
        final int driverAge = 24;
        final boolean hasAccidents = false;
        final Date firstRegistrationDate = DateUtility.convertToDate(LocalDate.now().minusYears(carAge));
        final OfferRequest input = new OfferRequest(firstRegistrationDate, carCc, driverAge, hasAccidents);
        final MultiCriteria criteria = mock(MultiCriteria.class);
        final double baseAmount = 10.0;
        final double expectedPremium = 10.0 * AGE_COEFFICIENT * TAX_COEFFICIENT;
        when(multiCriteriaRepository.getMultiCriteria(carCc, carAge)).thenReturn(Optional.of(criteria));
        when(criteria.getBaseAmount()).thenReturn(baseAmount);

        Premium result = sut.calculateTotalPremium(input);

        Assertions.assertEquals(expectedPremium, result.getTotalPremium(), RESULT_COMPARISON_DELTA);
    }

    @Test
    void calculateTotalPremium_Should_returnPremiumWithValue_When_repositoryReturnMultiCriteria_And_driverAgeIs_25() {
        final int carAge = 12;
        final int carCc = 1500;
        final int driverAge = 25;
        final boolean hasAccidents = false;
        final Date firstRegistrationDate = DateUtility.convertToDate(LocalDate.now().minusYears(carAge));
        final OfferRequest input = new OfferRequest(firstRegistrationDate, carCc, driverAge, hasAccidents);
        final MultiCriteria criteria = mock(MultiCriteria.class);
        final double baseAmount = 10.0;
        final double expectedPremium = 10.0 * TAX_COEFFICIENT;
        when(multiCriteriaRepository.getMultiCriteria(carCc, carAge)).thenReturn(Optional.of(criteria));
        when(criteria.getBaseAmount()).thenReturn(baseAmount);

        Premium result = sut.calculateTotalPremium(input);

        Assertions.assertEquals(expectedPremium, result.getTotalPremium(), RESULT_COMPARISON_DELTA);
    }

    @Test
    void calculateTotalPremium_Should_returnPremiumWithValue_When_repositoryReturnMultiCriteria_And_hasAccidents_And_driverAgeIs_25() {
        final int carAge = 12;
        final int carCc = 1500;
        final int driverAge = 25;
        final boolean hasAccidents = true;
        final Date firstRegistrationDate = DateUtility.convertToDate(LocalDate.now().minusYears(carAge));
        final OfferRequest input = new OfferRequest(firstRegistrationDate, carCc, driverAge, hasAccidents);
        final MultiCriteria criteria = mock(MultiCriteria.class);
        final double baseAmount = 10.0;
        final double expectedPremium = 10.0 * ACCIDENT_COEFFICIENT * TAX_COEFFICIENT;
        when(multiCriteriaRepository.getMultiCriteria(carCc, carAge)).thenReturn(Optional.of(criteria));
        when(criteria.getBaseAmount()).thenReturn(baseAmount);

        Premium result = sut.calculateTotalPremium(input);

        Assertions.assertEquals(expectedPremium, result.getTotalPremium(), RESULT_COMPARISON_DELTA);
    }

    @Test
    void calculateTotalPremium_Should_returnPremiumWithValue_When_repositoryReturnMultiCriteria_And_hasAccidents_And_driverAgeIs_24() {
        final int carAge = 12;
        final int carCc = 1500;
        final int driverAge = 24;
        final boolean hasAccidents = true;
        final Date firstRegistrationDate = DateUtility.convertToDate(LocalDate.now().minusYears(carAge));
        final OfferRequest input = new OfferRequest(firstRegistrationDate, carCc, driverAge, hasAccidents);
        final MultiCriteria criteria = mock(MultiCriteria.class);
        final double baseAmount = 10.0;
        final double expectedPremium = 10 * AGE_COEFFICIENT * ACCIDENT_COEFFICIENT * TAX_COEFFICIENT;
        when(multiCriteriaRepository.getMultiCriteria(carCc, carAge)).thenReturn(Optional.of(criteria));
        when(criteria.getBaseAmount()).thenReturn(baseAmount);

        Premium result = sut.calculateTotalPremium(input);

        Assertions.assertEquals(expectedPremium, result.getTotalPremium(), RESULT_COMPARISON_DELTA);
    }

    @Test
    void updateMultiCriteriaFromFile_Should_throwInvalidArgumentException_When_givenFileIsNotProperCVSFormat() throws IOException {
        final String fileContent = "someWierdFormat,ofText,ButNot,ProperCSV";
        final MultipartFile input = mock(MultipartFile.class);
        when(input.getBytes()).thenReturn(fileContent.getBytes());
        when(input.getInputStream()).thenReturn(new ByteArrayInputStream(fileContent.getBytes(StandardCharsets.UTF_8)));

        Assertions.assertThrows(InvalidArgumentException.class,
                () -> sut.updateMultiCriteriaFromFile(input));
    }

    @Test
    void updateMultiCriteriaFromFile_Should_clearOldItemsAndSaveTheNewItems_When_givenFileIsProperCVSFormat() throws IOException {
        final List<MultiCriteria> expectedCriteria = new ArrayList<>();
        expectedCriteria.add(MultiCriteria.builder()
                .baseAmount(123.34)
                .carAgeMax(19)
                .carAgeMin(340)
                .ccMax(1047)
                .ccMin(31232)
                .build());
        expectedCriteria.add(MultiCriteria.builder()
                .baseAmount(413.25)
                .carAgeMax(999)
                .carAgeMin(20)
                .ccMax(1047)
                .ccMin(0)
                .build());
        final String fileContent =
                "\"123.34\",\"19\",\"340\",\"1047\",\"31232\"\n" +
                        "\"413.25\",\"999\",\"20\",\"1047\",\"0\"";
        final MultipartFile input = mock(MultipartFile.class);
        when(input.getBytes()).thenReturn(fileContent.getBytes());
        when(input.getInputStream()).thenReturn(new ByteArrayInputStream(fileContent.getBytes(StandardCharsets.UTF_8)));

        sut.updateMultiCriteriaFromFile(input);

        InOrder inOrder = inOrder(multiCriteriaRepository);
        inOrder.verify(multiCriteriaRepository).deleteAll();
        inOrder.verify(multiCriteriaRepository).saveAll(expectedCriteria);
    }

    @Test
    void getCurrentMultiCriteria_Should_returnResultFromRepository() {
        Set<MultiCriteria> expected = mock(Set.class);
        when(multiCriteriaRepository.findAll()).thenReturn(expected);

        Set<MultiCriteria> result = sut.getCurrentMultiCriteria();
        Assertions.assertEquals(expected, result);
    }
}