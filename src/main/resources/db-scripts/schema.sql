create schema if not exists safety_car;

create or replace table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    UNIQUE KEY username_authority (username, authority)
);

create or replace table users
(
    username varchar(50) not null,
    password varchar(50) not null,
    enabled  tinyint(4)  not null,
    PRIMARY KEY (username)
);


create or replace table policies
(
    policy_id                bigint       not null auto_increment,
    city                     varchar(255) not null,
    creation_date            date         not null,
    cubic_capacity           integer,
    driver_age               integer,
    effective_date           date         not null,
    email                    varchar(255) not null,
    previous_accidents       bit          not null,
    phone                    varchar(255) not null,
    policy_number            varchar(255) not null,
    registration_certificate varchar(255) not null,
    registration_date        date         not null,
    status                   varchar(255) not null,
    street                   varchar(255) not null,
    total                    double precision,
    model_id                 bigint       not null,
    customer_id              varchar(255) not null,
    primary key (policy_id)
);

create or replace table customers
(
    customer_id varchar(255) not null,
    city        varchar(255),
    activated   bit,
    phone       varchar(255),
    street      varchar(255),
    primary key (customer_id)
);

create or replace table multi_criteria
(
    criteria_id bigint not null auto_increment,
    base_amount double precision,
    car_age_max integer,
    car_age_min integer,
    cc_max      integer,
    cc_min      integer,
    primary key (criteria_id)
);

create or replace table vehicle_models
(
    model_id bigint not null auto_increment,
    name     varchar(255),
    brand_id bigint,
    primary key (model_id)
);

create or replace table vehicle_brands
(
    brand_id bigint not null auto_increment,
    name     varchar(255),
    primary key (brand_id)
);

alter table policies
    add constraint FK_policies_model foreign key (model_id) references vehicle_models (model_id);
alter table policies
    add constraint FK_policies_customer foreign key (customer_id) references customers (customer_id);
alter table vehicle_models
    add constraint FK_models foreign key (brand_id) references vehicle_brands (brand_id);
alter table authorities
    add constraint FK_security FOREIGN KEY (username) REFERENCES users (username);

