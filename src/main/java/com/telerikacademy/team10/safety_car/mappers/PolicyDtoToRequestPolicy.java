package com.telerikacademy.team10.safety_car.mappers;

import com.telerikacademy.team10.safety_car.dto.RequestPolicyDataDto;
import com.telerikacademy.team10.safety_car.dto.RequestPolicyDto;
import com.telerikacademy.team10.safety_car.models.PolicyRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PolicyDtoToRequestPolicy {

    PolicyRequest dtoToEntity(RequestPolicyDataDto dto);

    PolicyRequest dtoToEntity(RequestPolicyDto dto);

    RequestPolicyDataDto entityToDto(PolicyRequest policyRequest);
}
