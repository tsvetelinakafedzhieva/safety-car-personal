package com.telerikacademy.team10.safety_car.mappers;

import com.telerikacademy.team10.safety_car.dto.CustomerDto;
import com.telerikacademy.team10.safety_car.models.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

    CustomerDto entityToDto(Customer source);

    Customer dtoToEntity(CustomerDto destination);
}
