package com.telerikacademy.team10.safety_car.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class UpdateCustomerDto {

    private static final String ADDRESS_LENGTH_MESSAGE = "Address should be between 3 and 50 symbols.";

    @Size(min = 3, max = 50, message = ADDRESS_LENGTH_MESSAGE)
    private String city;

    @Size(min = 3, max = 50, message = ADDRESS_LENGTH_MESSAGE)
    private String street;

    @Pattern(regexp = RequestPolicyDto.PATTERN)
    private String phone;

    @Nullable
    private String oldPassword;

    @Nullable
    private String password;

    @Nullable
    private String passwordConfirmation;
}
