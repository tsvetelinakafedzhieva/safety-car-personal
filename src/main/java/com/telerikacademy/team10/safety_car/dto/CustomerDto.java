package com.telerikacademy.team10.safety_car.dto;

import com.telerikacademy.team10.safety_car.validations.PasswordMatches;
import com.telerikacademy.team10.safety_car.validations.ValidEmail;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@PasswordMatches
public class CustomerDto {

    private static final String EMAIL_LENGTH_MESSAGE = "Customer email should be between 10 and 50 symbols.";
    private static final String PASSWORD_LENGTH_MESSAGE = "Password should be between 5 and 50 symbols.";

    @ValidEmail
    @Size(min = 10, max = 50, message = EMAIL_LENGTH_MESSAGE)
    private String email;

    @NotNull
    @NotEmpty
    @Size(min = 5, max = 50, message = PASSWORD_LENGTH_MESSAGE)
    private String password;

    @NotNull
    @NotEmpty
    @Size(min = 5, max = 50, message = PASSWORD_LENGTH_MESSAGE)
    private String passwordConfirmation;
}