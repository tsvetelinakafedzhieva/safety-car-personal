package com.telerikacademy.team10.safety_car.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Table(name = "multi_criteria")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonPropertyOrder({"baseAmount", "carAgeMax", "carAgeMin", "ccMax", "ccMin"})
public class MultiCriteria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "criteria_id")
    private long id;

    @Column(name = "cc_min")
    private int ccMin;

    @Column(name = "cc_max")
    private int ccMax;

    @Column(name = "car_age_min")
    private int carAgeMin;

    @Column(name = "car_age_max")
    private int carAgeMax;

    @Column(name = "base_amount")
    private double baseAmount;

}
