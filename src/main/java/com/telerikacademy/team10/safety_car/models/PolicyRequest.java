package com.telerikacademy.team10.safety_car.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

@Data
@AllArgsConstructor
public class PolicyRequest {

    private Date effectiveDate;

    private long modelId;

    private Date registrationDate;

    private int cubicCapacity;

    private Boolean hasPreviousAccidents;

    private MultipartFile registrationCertificate;

    private int driverAge;

    private double totalAmount;

    private String phone;

    private String email;

    private String city;

    private String street;

    private String policyNumber;

}
