package com.telerikacademy.team10.safety_car.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "vehicle_brands")
public class VehicleBrand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "brand_id")
    private long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "brand")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Set<VehicleModel> vehicleModels;

    public String modelsAsJson() {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(vehicleModels);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "[]";
        }
    }

    public boolean hasModelWithId(long modelId) {
        return vehicleModels.stream().anyMatch(vehicleModel -> vehicleModel.getId() == modelId);
    }
}
