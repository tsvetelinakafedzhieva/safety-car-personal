package com.telerikacademy.team10.safety_car.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "policies")
public class Policy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "policy_id")
    private long id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    @JoinColumn(name = "customer_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Customer owner;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "effective_date")
    private Date effectiveDate;

    @JsonIgnore
    @NotNull
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "model_id")
    private VehicleModel model;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "registration_date")
    private Date registrationDate;

    @JsonIgnore
    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @NotNull
    @Column(name = "previous_accidents")
    private boolean hasPreviousAccidents;

    @NotNull
    @Column(name = "registration_certificate")
    private String registrationCertificate;

    @Column(name = "driver_age")
    private int driverAge;

    @Column(name = "total")
    private double totalAmount;

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private StatusType status;

    @NotNull
    @Column(name = "email")
    private String email;

    @NotNull
    @Column(name = "phone")
    private String phone;

    @NotNull
    @Column(name = "city")
    private String city;

    @NotNull
    @Column(name = "street")
    private String street;

    @NotNull
    @Column(name = "creation_date")
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @NotNull
    @Column(name = "policy_number")
    private String policyNumber;

}
