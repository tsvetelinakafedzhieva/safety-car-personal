package com.telerikacademy.team10.safety_car.models.enums;

import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;

public enum FilterType {

    ANY,
    USERNAME,
    DATE,
    STATUS,
    NUMBER;

    private final static String ERROR_FILTER = "Invalid filter";

    public static FilterType filterTypeFromString(String type) {
        switch (type.toUpperCase()) {
            case "USERNAME":
                return USERNAME;
            case "DATE":
                return DATE;
            case "STATUS":
                return STATUS;
            case "NUMBER":
                return NUMBER;
            case "ANY":
                return ANY;
            default:
                throw new InvalidArgumentException(ERROR_FILTER);
        }
    }

}
