package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.exceptions.InvalidOperationException;
import com.telerikacademy.team10.safety_car.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.safety_car.factories.PolicyFactory;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.PolicyRequest;
import com.telerikacademy.team10.safety_car.models.enums.FilterType;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;
import com.telerikacademy.team10.safety_car.repositories.contracts.CustomerRepository;
import com.telerikacademy.team10.safety_car.repositories.contracts.PolicyRepository;
import com.telerikacademy.team10.safety_car.services.contracts.AuthenticationService;
import com.telerikacademy.team10.safety_car.services.contracts.PolicyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Set;

@Service
public class PolicyServiceImpl implements PolicyService {

    private final static String BAD_USER_ROLE_MESSAGE = "Can't provide policies of unknown user role";
    private final static String POLICY_NOT_FOUND = "Policy with ID %d was not found";
    private final static String INVALID_OPERATION = "Trying to do invalid operation to policy";
    private final static String UNAUTHORIZED_MESSAGE = "You need permission to perform this action";
    private final static String FILTER_MESSAGE = "Required both type and key for filter actions.";

    private final PolicyRepository policyRepository;
    private final PolicyFactory policyFactory;
    private final AuthenticationService authenticationService;
    private final CustomerRepository customerRepository;

    @Autowired
    public PolicyServiceImpl(PolicyRepository policyRepository,
                             PolicyFactory policyFactory,
                             AuthenticationService authenticationService,
                             CustomerRepository customerRepository) {
        this.policyRepository = policyRepository;
        this.policyFactory = policyFactory;
        this.authenticationService = authenticationService;
        this.customerRepository = customerRepository;
    }

    /**
     * Returns all policies for the provided user.
     * If user's role is agent, it will return all policies in the system with pending status.
     * If user's role is customer, it will return all of his policies no matter of the policy status.
     * Policies can be filtered by different criteria. In this case are required both arguments: filterType and filterValue.
     *
     * @param email must not be {@literal null}.
     * @param filterType by which it will filter the policies {@code Optional}.
     * @param filterValue is the concrete value for the chosen filter {@code Optional}.
     * @throws InvalidArgumentException in case one of the arguments filterType or filterValue is present but
     * the other one is empty.
     * @throws InvalidArgumentException in case user's role is unknown.
     * @return all policies in case there are no policies it will return empty collection
     */
    @Override
    public Set<Policy> getPoliciesForUser(String email, Optional<String> filterType, Optional<String> filterValue) {
        if (filterType.isEmpty() && filterValue.isPresent() || filterType.isPresent() && filterValue.isEmpty()) {
            throw new InvalidArgumentException(FILTER_MESSAGE);
        }

        Optional<Customer> optionalOwner;
        if (authenticationService.isCustomer(email)) {
            optionalOwner = customerRepository.findById(email);
        } else if (authenticationService.isAgent(email)) {
            optionalOwner = Optional.empty();
        } else {
            throw new InvalidArgumentException(BAD_USER_ROLE_MESSAGE);
        }

        FilterType filter = FilterType.ANY;
        String keyValue = "";
        if (filterType.isPresent()) {
            filter = FilterType.filterTypeFromString(filterType.get());
            keyValue = filterValue.get();
        }

        return policyRepository.getAllPolicies(optionalOwner, filter, keyValue);
    }

    @Override
    public Policy getById(long id) {
        Optional<Policy> policy = policyRepository.findById(id);
        if (policy.isEmpty()) {
            throw new EntityNotFoundException(String.format(POLICY_NOT_FOUND, id));
        }
        return policy.get();
    }

    @Override
    public void createPolicy(PolicyRequest policyRequest, String ownerId) {
        Policy policy = policyFactory.create(policyRequest, ownerId);
        policyRepository.save(policy);
    }

    @Override
    public void updatePolicyStatus(long policyId, String username, StatusType status) {
        switch (status) {
            case ACCEPTED:
            case REJECTED:
                if (!authenticationService.isAgent(username)) {
                    throw new UnauthorizedAccessException(UNAUTHORIZED_MESSAGE);
                }
                break;
            case PENDING:
                throw new InvalidOperationException(INVALID_OPERATION);
            case CANCELED:
                if (!authenticationService.isCustomer(username)) {
                    throw new UnauthorizedAccessException(UNAUTHORIZED_MESSAGE);
                }
        }

        Policy policy = getById(policyId);

        if (policy.getStatus() != StatusType.PENDING) {
            throw new InvalidOperationException(INVALID_OPERATION);
        }

        policy.setStatus(status);
        policyRepository.save(policy);
    }

    @Override
    public String generatePolicyNumber(Customer customer) {
        LocalDate localDate = LocalDate.now();
        String prefix = localDate.format(DateTimeFormatter.ISO_DATE);
        int consecutivePolicy = customer.getPolicies().size() + 1;
        return String.format("%s/%d", prefix, consecutivePolicy);
    }

}
