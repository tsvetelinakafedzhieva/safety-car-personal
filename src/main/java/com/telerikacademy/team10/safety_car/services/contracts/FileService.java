package com.telerikacademy.team10.safety_car.services.contracts;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    String storeImage(MultipartFile file);

    Resource loadFileAsResource(String fileName);

}
