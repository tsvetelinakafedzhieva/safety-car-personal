package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.safety_car.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.safety_car.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.repositories.contracts.AuthenticationRepository;
import com.telerikacademy.team10.safety_car.repositories.contracts.CustomerRepository;
import com.telerikacademy.team10.safety_car.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final AuthenticationRepository authenticationRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, AuthenticationRepository authenticationRepository) {
        this.customerRepository = customerRepository;
        this.authenticationRepository = authenticationRepository;
    }

    @Override
    public Customer getByEmail(String email) {
        Optional<Customer> customer = customerRepository.findById(email);
        if (customer.isEmpty()) {
            throw new EntityNotFoundException(String.format("Customer with email %s not found!", email));
        }
        return customer.get();
    }

    @Override
    @Transactional
    public void create(Customer customer) {
        if (authenticationRepository.exists(customer.getEmail())) {
            throw new DuplicateEntityException(String.format("Customer with email %s already exists!", customer.getEmail()));
        }
        customerRepository.save(customer);
    }

    @Override
    @Transactional
    public void update(String email, Customer customer) {
        if (!authenticationRepository.isAuthenticated(email)) {
            throw new UnauthorizedAccessException("Trying to change account settings of someone else.");
        }
        Customer customerToUpdate = getByEmail(email);
        customerToUpdate.setCity(customer.getCity());
        customerToUpdate.setStreet(customer.getStreet());
        customerToUpdate.setPhone(customer.getPhone());
        customerRepository.save(customerToUpdate);
    }

    @Override
    public List<Policy> getPolicies(String email) {
        Optional<Customer> customer = customerRepository.findById(email);
        if (customer.isEmpty()) {
            throw new EntityNotFoundException(String.format("Customer with email %s not found!", email));
        }
        List<Policy> customerPolicies = customer.get().getPolicies().stream().sorted(Comparator.comparing(Policy::getCreationDate)).collect(Collectors.toList());
        return customerPolicies;
    }
}
