package com.telerikacademy.team10.safety_car.services.contracts;

import com.telerikacademy.team10.safety_car.models.MultiCriteria;
import com.telerikacademy.team10.safety_car.models.OfferRequest;
import com.telerikacademy.team10.safety_car.models.Premium;

import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

public interface MultiCriteriaService {

    Premium calculateTotalPremium(OfferRequest offerRequest);

    void updateMultiCriteriaFromFile(MultipartFile file);

    Set<MultiCriteria> getCurrentMultiCriteria();
}
