package com.telerikacademy.team10.safety_car.services.contracts;

public interface AuthenticationService {

    void register(String username, String password);

    void changePassword(String username, String oldPassword, String newPassword, String passwordConfirmation);

    boolean isAuthenticated(String username);

    boolean isCustomer(String username);

    boolean isAgent(String username);
}
