package com.telerikacademy.team10.safety_car.services.contracts;

import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.PolicyRequest;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;

import java.util.Optional;
import java.util.Set;

public interface PolicyService {

    Set<Policy> getPoliciesForUser(String email, Optional<String> type, Optional<String> key);

    Policy getById(long id);

    String generatePolicyNumber(Customer customer);

    void createPolicy(PolicyRequest policyRequest, String ownerId);

    void updatePolicyStatus(long policyId, String email, StatusType status);

}
