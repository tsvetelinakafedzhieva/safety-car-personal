package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.safety_car.models.enums.RoleType;
import com.telerikacademy.team10.safety_car.repositories.contracts.AuthenticationRepository;
import com.telerikacademy.team10.safety_car.services.contracts.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final static String USER_ERROR_MESSAGE = "That User already exists in the system";
    private final static String PASSWORD_ERROR_MESSAGE = "Trying to change password of someone else.";
    private final static String PASSWORD_MATCH_ERROR_MESSAGE = "Password and password confirmation doesn't match!";

    private final AuthenticationRepository authenticationRepository;

    @Autowired
    public AuthenticationServiceImpl(AuthenticationRepository authenticationRepository) {
        this.authenticationRepository = authenticationRepository;
    }

    @Override
    public void register(String username, String password) {
        if (authenticationRepository.exists(username)) {
            throw new DuplicateEntityException(USER_ERROR_MESSAGE);
        }
        authenticationRepository.create(username, password, RoleType.ROLE_CUSTOMER);
    }

    @Override
    @Transactional
    public void changePassword(String username, String oldPassword, String newPassword, String passwordConfirmation) {
        Optional<String> original = authenticationRepository.getPasswordFor(username);
        if (original.isPresent() && oldPassword.equals(original.get())) {
            if (!isAuthenticated(username)) {
                throw new UnauthorizedAccessException(PASSWORD_ERROR_MESSAGE);
            }
            if (!newPassword.equals(passwordConfirmation)) {
                throw new InvalidArgumentException(PASSWORD_MATCH_ERROR_MESSAGE);
            }
            authenticationRepository.updatePassword(oldPassword, newPassword);
        } else {
            throw new UnauthorizedAccessException(PASSWORD_ERROR_MESSAGE);
        }
    }

    @Override
    public boolean isAuthenticated(String username) {
        return authenticationRepository.isAuthenticated(username);
    }

    @Override
    public boolean isCustomer(String username) {
        return isAuthenticated(username) &&
                authenticationRepository.getRoles(username).contains(RoleType.ROLE_CUSTOMER);
    }

    @Override
    public boolean isAgent(String username) {
        return isAuthenticated(username) &&
                authenticationRepository.getRoles(username).contains(RoleType.ROLE_AGENT);
    }
}
