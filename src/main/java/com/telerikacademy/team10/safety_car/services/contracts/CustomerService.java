package com.telerikacademy.team10.safety_car.services.contracts;

import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;

import java.util.List;

public interface CustomerService {

    Customer getByEmail(String email);

    void create(Customer customer);

    void update(String email, Customer customer);

    List<Policy> getPolicies(String email);
}
