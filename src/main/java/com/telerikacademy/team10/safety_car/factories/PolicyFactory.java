package com.telerikacademy.team10.safety_car.factories;

import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.PolicyRequest;

public interface PolicyFactory {

    Policy create(PolicyRequest policyRequest, String ownerId);
}
