package com.telerikacademy.team10.safety_car.exceptions;

public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(final String message) {
        super(message);
    }
}
