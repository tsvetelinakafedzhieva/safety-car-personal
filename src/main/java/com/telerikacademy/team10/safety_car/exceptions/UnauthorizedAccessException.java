package com.telerikacademy.team10.safety_car.exceptions;

public class UnauthorizedAccessException extends RuntimeException {

    public UnauthorizedAccessException(final String message) {
        super(message);
    }
}
