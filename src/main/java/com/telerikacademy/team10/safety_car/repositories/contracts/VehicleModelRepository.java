package com.telerikacademy.team10.safety_car.repositories.contracts;

import com.telerikacademy.team10.safety_car.models.VehicleModel;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface VehicleModelRepository extends CrudRepository<VehicleModel, Long> {

    @Override
    Set<VehicleModel> findAll();
}
