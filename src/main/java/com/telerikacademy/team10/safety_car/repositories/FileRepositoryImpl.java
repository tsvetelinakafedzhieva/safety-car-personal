package com.telerikacademy.team10.safety_car.repositories;

import com.telerikacademy.team10.safety_car.configuration.FileStorageProperties;
import com.telerikacademy.team10.safety_car.exceptions.FileStorageException;
import com.telerikacademy.team10.safety_car.repositories.contracts.FileRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.Optional;

@Repository
public class FileRepositoryImpl implements FileRepository {

    private final static String FILE_DIRECTORY = "Could not create the directory where the uploaded files will be stored.";

    private final Path fileStorageLocation;

    @Autowired
    public FileRepositoryImpl(FileStorageProperties fileStorageProperties) {
        fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        try {
            Files.createDirectories(fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException(FILE_DIRECTORY, ex);
        }
    }

    @Override
    public Optional<String> saveFile(MultipartFile file) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            if (fileName.contains("..")) {
                return Optional.empty();
            }

            fileName = String.format("%d-%s", System.currentTimeMillis(), fileName);
            Path targetLocation = fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return Optional.of(fileName);
        } catch (IOException ex) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Resource> getAsResource(String fileName) {
        try {
            Path filePath = fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return Optional.of(resource);
            } else {
                return Optional.empty();
            }
        } catch (MalformedURLException ex) {
            return Optional.empty();
        }
    }
}
