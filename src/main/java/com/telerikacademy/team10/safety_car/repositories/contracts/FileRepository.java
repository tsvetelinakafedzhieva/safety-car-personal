package com.telerikacademy.team10.safety_car.repositories.contracts;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

public interface FileRepository {

    Optional<String> saveFile(MultipartFile file);

    Optional<Resource> getAsResource(String fileName);

}
