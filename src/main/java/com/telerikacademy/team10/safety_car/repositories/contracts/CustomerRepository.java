package com.telerikacademy.team10.safety_car.repositories.contracts;

import com.telerikacademy.team10.safety_car.models.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, String> {

    @Override
    Set<Customer> findAll();
}
