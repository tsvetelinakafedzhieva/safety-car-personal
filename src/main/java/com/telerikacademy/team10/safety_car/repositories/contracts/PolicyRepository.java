package com.telerikacademy.team10.safety_car.repositories.contracts;

import com.telerikacademy.team10.safety_car.models.Policy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PolicyRepository extends CrudRepository<Policy, Long>, PolicyCustomRepository {

    @Override
    List<Policy> findAll();

}
