package com.telerikacademy.team10.safety_car.utility;

import org.springframework.lang.NonNull;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateUtility {

    private DateUtility() {

    }

    @NonNull
    public static Date convertToDate(@NonNull LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
