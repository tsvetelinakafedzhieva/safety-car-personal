package com.telerikacademy.team10.safety_car.controllers.web;

import com.telerikacademy.team10.safety_car.services.contracts.MultiCriteriaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/admin")
class AgentWebController {

    private final MultiCriteriaService multiCriteriaService;

    @Autowired
    AgentWebController(MultiCriteriaService multiCriteriaService) {
        this.multiCriteriaService = multiCriteriaService;
    }

    @PreAuthorize("hasRole('AGENT')")
    @GetMapping("/criteria")
    public String showCurrentCriteria(Model model) {
        model.addAttribute("criterias", multiCriteriaService.getCurrentMultiCriteria());
        return "criteria";
    }

    @PreAuthorize("hasRole('AGENT')")
    @PostMapping("/criteria")
    public String updateCriteria(@RequestPart("file") MultipartFile file, Model model) {
        multiCriteriaService.updateMultiCriteriaFromFile(file);

        return showCurrentCriteria(model);
    }
}
