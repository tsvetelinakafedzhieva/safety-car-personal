package com.telerikacademy.team10.safety_car.controllers.web;

import com.telerikacademy.team10.safety_car.configuration.SessionConstants;
import com.telerikacademy.team10.safety_car.dto.CustomerDto;
import com.telerikacademy.team10.safety_car.dto.RequestOfferDto;
import com.telerikacademy.team10.safety_car.mappers.CustomerMapper;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.services.contracts.AuthenticationService;
import com.telerikacademy.team10.safety_car.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
public class AuthenticationController {
    private final CustomerService customerService;
    private final CustomerMapper customerMapper;
    private final AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(CustomerService customerService,
                                    CustomerMapper customerMapper,
                                    AuthenticationService authenticationService) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
        this.authenticationService = authenticationService;
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/login")
    public String showLoginPage() {
        return "login";
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/login/offer")
    public String loginWithPolicyOffer(
            @ModelAttribute RequestOfferDto dto,
            HttpSession session
    ) {
        saveToSession(dto, session);
        return showLoginPage();
    }


    @PreAuthorize("permitAll()")
    @PostMapping("/register/offer")
    public String registerWithPolicyOffer(
            @ModelAttribute RequestOfferDto dto,
            Model model,
            HttpSession session
    ) {
        saveToSession(dto, session);
        return showRegisterPage(model);
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("customer", new CustomerDto());
        return "register";
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/register")
    public String registerCustomer(@Valid @ModelAttribute CustomerDto customerDto, BindingResult result, Model model) {
        if(result.hasErrors()) {
            model.addAttribute("customer", new CustomerDto());
            model.addAttribute("passwordMessage", "Passwords do not match.");
            return "register";
        }

        Customer customer = customerMapper.dtoToEntity(customerDto);
        customerService.create(customer);
        authenticationService.register(customerDto.getEmail(), customerDto.getPassword());


        return "register-confirmation";
    }

    private void saveToSession(@NonNull RequestOfferDto dto, @NonNull HttpSession session) {
        session.setAttribute(SessionConstants.KEY_SESSION_POLICY_OFFER, dto);
        session.setAttribute(SessionConstants.KEY_SESSION_POST_LOGIN_DESTINATION, PolicyWebController.OFFER_MAPPING);
    }
}
