package com.telerikacademy.team10.safety_car.controllers;

import com.telerikacademy.team10.safety_car.dto.CustomerDto;
import com.telerikacademy.team10.safety_car.exceptions.DuplicateEntityException;
import com.telerikacademy.team10.safety_car.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.safety_car.exceptions.UnauthorizedAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(annotations = Controller.class)
public class ExceptionHandlingControllerAdviceMVC {

    public static final String DEFAULT_ERROR_VIEW = "default-error-view";
    public static final String REGISTERED_ERROR_VIEW = "register";

    public static final String UNAUTHORIZED_ERROR_MESSAGE = "Access denied";
    public static final String UNAUTHORIZED_ERROR_NOTE = "You do not have permission to access this page. " +
            "Please, contact your Site Administrator(s) to request access.";


    public static final String NOT_FOUND_ERROR_MESSAGE = "Page not found";
    public static final String NOT_FOUND_ERROR_NOTE = "The page you are looking for is not found. " +
            "Please, make sure you have typed the correct URL.";

    public static final String CONFLICT_USER_ALREADY_EXISTS = "This email was already registered";

    @ExceptionHandler(UnauthorizedAccessException.class)
    public ModelAndView handleUnauthorized() {
        return getModelAndView(UNAUTHORIZED_ERROR_MESSAGE, UNAUTHORIZED_ERROR_NOTE);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ModelAndView handleNotFound() {
        return getModelAndView(NOT_FOUND_ERROR_MESSAGE, NOT_FOUND_ERROR_NOTE);
    }

    @ExceptionHandler(DuplicateEntityException.class)
    public ModelAndView handleConflict() {
        ModelAndView mav = new ModelAndView(REGISTERED_ERROR_VIEW);
        mav.addObject("registeredEmailError", CONFLICT_USER_ALREADY_EXISTS);
        mav.addObject("customer", new CustomerDto());
        return mav;
    }

    private ModelAndView getModelAndView(String message, String note) {
        ModelAndView mav = new ModelAndView(DEFAULT_ERROR_VIEW);
        mav.addObject("message", message);
        mav.addObject("note", note);
        return mav;
    }

}
