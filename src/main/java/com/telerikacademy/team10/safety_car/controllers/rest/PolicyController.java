package com.telerikacademy.team10.safety_car.controllers.rest;

import com.telerikacademy.team10.safety_car.dto.RequestOfferDto;
import com.telerikacademy.team10.safety_car.dto.RequestPolicyDataDto;
import com.telerikacademy.team10.safety_car.dto.StatusDto;
import com.telerikacademy.team10.safety_car.mappers.OfferDtoToOfferDomain;
import com.telerikacademy.team10.safety_car.mappers.PolicyDtoToRequestPolicy;
import com.telerikacademy.team10.safety_car.models.OfferRequest;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.PolicyRequest;
import com.telerikacademy.team10.safety_car.models.Premium;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;
import com.telerikacademy.team10.safety_car.services.contracts.MultiCriteriaService;
import com.telerikacademy.team10.safety_car.services.contracts.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/policies")
public class PolicyController {

    private final MultiCriteriaService multiCriteriaService;
    private final OfferDtoToOfferDomain offerMapper;
    private final PolicyDtoToRequestPolicy policyMapper;
    private final PolicyService policyService;


    @Autowired
    public PolicyController(MultiCriteriaService multiCriteriaService,
                            OfferDtoToOfferDomain offerMapper,
                            PolicyDtoToRequestPolicy policyMapper,
                            PolicyService policyService) {
        this.multiCriteriaService = multiCriteriaService;
        this.offerMapper = offerMapper;
        this.policyMapper = policyMapper;
        this.policyService = policyService;
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/offer")
    public Premium calculateOffer(@Valid @RequestBody RequestOfferDto dto) {
        OfferRequest request = offerMapper.dtoToDomain(dto);
        return multiCriteriaService.calculateTotalPremium(request);
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @PostMapping("/new")
    public void create(@Valid @RequestPart("policyData") RequestPolicyDataDto policyDto,
                       @Valid @RequestPart("registrationCertificate") MultipartFile multipartFile,
                       Principal principal) {
        PolicyRequest policyRequest = policyMapper.dtoToEntity(policyDto);
        policyRequest.setRegistrationCertificate(multipartFile);
        policyService.createPolicy(policyRequest, principal.getName());
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping
    public Set<Policy> getAll(@RequestParam(required = false) String type,
                              @RequestParam(required = false) String key,
                              Principal principal) {
        return policyService.getPoliciesForUser(
                principal.getName(),
                Optional.ofNullable(type),
                Optional.ofNullable(key)
        );
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/{id}")
    public Policy getById(@PathVariable long id) {
        return policyService.getById(id);
    }

    @PreAuthorize("isAuthenticated()")
    @PutMapping("/{id}")
    public void updateStatus(@PathVariable long id, @Valid @RequestBody StatusDto dto, Principal principal) {
        StatusType statusType = StatusType.statusTypeFromString(dto.getStatus());
        policyService.updatePolicyStatus(id, principal.getName(), statusType);

    }

}
