package com.telerikacademy.team10.safety_car.controllers.rest;

import com.telerikacademy.team10.safety_car.dto.CustomerDto;
import com.telerikacademy.team10.safety_car.dto.UpdateCustomerDto;
import com.telerikacademy.team10.safety_car.mappers.CustomerMapper;
import com.telerikacademy.team10.safety_car.mappers.UpdateCustomerMapper;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.services.contracts.AuthenticationService;
import com.telerikacademy.team10.safety_car.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {

    private final CustomerService customerService;
    private final CustomerMapper customerMapper;
    private final UpdateCustomerMapper updateCustomerMapper;
    private final AuthenticationService authenticationService;

    @Autowired
    public CustomerRestController(CustomerService customerService, CustomerMapper customerMapper, UpdateCustomerMapper updateCustomerMapper, AuthenticationService authenticationService) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
        this.updateCustomerMapper = updateCustomerMapper;
        this.authenticationService = authenticationService;
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @GetMapping("/{email}")
    public Customer getByEmail(@PathVariable String email) {
        return customerService.getByEmail(email);
    }

    @PreAuthorize("permitAll()")
    @PostMapping
    public void create(@Valid @RequestBody CustomerDto customerDto) {
        Customer customer = customerMapper.dtoToEntity(customerDto);
        customerService.create(customer);
        authenticationService.register(customerDto.getEmail(), customerDto.getPassword());
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @PutMapping("/{email}")
    public void update(@PathVariable String email,
                       @Valid @RequestBody UpdateCustomerDto updateCustomerDto) {
        Customer customerToUpdate = updateCustomerMapper.fromDto(updateCustomerDto);
        customerService.update(email, customerToUpdate);
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @GetMapping("/profile/customerPolicies")
    public List<Policy> getPolicies(Principal principal) {
        String email = principal.getName();
        return customerService.getPolicies(email);
    }
}
