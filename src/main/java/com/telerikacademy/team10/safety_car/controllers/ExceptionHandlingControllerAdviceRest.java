package com.telerikacademy.team10.safety_car.controllers;

import com.telerikacademy.team10.safety_car.exceptions.*;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = RestController.class)
public class ExceptionHandlingControllerAdviceRest extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {DuplicateEntityException.class})
    protected ResponseEntity<Object> handleConflict(DuplicateEntityException ex, WebRequest request) {
        return defaultWithStatus(ex, request, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = {EntityNotFoundException.class, FileNotFoundException.class})
    protected ResponseEntity<Object> handleNotFound(RuntimeException ex, WebRequest request) {
        return defaultWithStatus(ex, request, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {InvalidArgumentException.class, InvalidOperationException.class})
    protected ResponseEntity<Object> handleBadRequest(RuntimeException ex, WebRequest request) {
        return defaultWithStatus(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UnauthorizedAccessException.class)
    protected ResponseEntity<Object> handleUnauthorized(UnauthorizedAccessException ex, WebRequest request) {
        return defaultWithStatus(ex, request, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        return defaultWithStatus(ex, request, HttpStatus.FORBIDDEN);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return defaultWithStatus(ex, request, HttpStatus.BAD_REQUEST);
    }

    @NonNull
    private ResponseEntity<Object> defaultWithStatus(Exception ex, WebRequest request, HttpStatus status) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), status, request);
    }

}
