package com.telerikacademy.team10.safety_car.configuration.security;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;

@Configuration
public class AuthRedirectConfig {

    final static String DEFAULT_REDIRECTION = "/policies";

    @Bean
    public RedirectStrategy redirectStrategy() {
        return new DefaultRedirectStrategy();
    }

    @Bean
    @Qualifier("defaultLoginRedirectTarget")
    public String defaultLoginRedirectTarget() {
        return DEFAULT_REDIRECTION;
    }
}
