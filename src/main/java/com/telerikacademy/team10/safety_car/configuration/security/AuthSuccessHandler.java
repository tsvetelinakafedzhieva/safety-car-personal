package com.telerikacademy.team10.safety_car.configuration.security;

import com.telerikacademy.team10.safety_car.configuration.SessionConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component
class AuthSuccessHandler implements AuthenticationSuccessHandler {

    private final RedirectStrategy redirectStrategy;
    private final String loginRedirectTarget;

    @Autowired
    public AuthSuccessHandler(RedirectStrategy redirectStrategy,
                              @Qualifier("defaultLoginRedirectTarget") String loginRedirectTarget) {
        this.redirectStrategy = redirectStrategy;
        this.loginRedirectTarget = loginRedirectTarget;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        if (response.isCommitted()) {
            return;
        }

        HttpSession session = request.getSession();
        String destinationTarget = getRedirectionDestination(session);
        if (destinationTarget == null) {
            destinationTarget = loginRedirectTarget;
        }

        redirectStrategy.sendRedirect(request, response, destinationTarget);
    }

    @Nullable
    private String getRedirectionDestination(HttpSession session) {
        String redirectDestination = null;
        if (session != null && !session.isNew()) {
            redirectDestination =
                    (String) session.getAttribute(SessionConstants.KEY_SESSION_POST_LOGIN_DESTINATION);
        }
        return redirectDestination;
    }
}
