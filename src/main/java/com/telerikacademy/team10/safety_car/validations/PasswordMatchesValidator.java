package com.telerikacademy.team10.safety_car.validations;

import com.telerikacademy.team10.safety_car.dto.CustomerDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
    public void initialize(PasswordMatches constraint) {
    }

    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        CustomerDto dto = (CustomerDto) obj;
        return dto.getPassword().equals(dto.getPasswordConfirmation());
    }

}
